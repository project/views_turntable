


/*

Passed fron Drupal

viewsTurntableSettings.rearOpacity
viewsTurntableSettings.frontOpacity
viewsTurntableSettings.enabledSector
viewsTurntableSettings.idleSpinSpees
viewsTurntableSettings.idleInterval
viewsTurntableSettings.spinType
viewsTurntableSettings.spinSpeed
viewsTurntableSettings.nodeWidth
viewsTurntableSettings.ellipseHeight

*/



NodeRotator.profileSmooth = new Array(1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2.5, 2.25, 2, 1.5, 1, 0.25);
NodeRotator.profileSmoothStart = new Array(1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1);
NodeRotator.profileFlat =   new Array(3, 3, 3);


function NodeRotator(parent){

  // Get visual elements
  this.parent = parent;
  this.nodes = $('li.turntable-node', this.parent);
  this.nodeObjects = new Array();
  for(var i = 0; i < this.nodes.length; i++){
    this.nodeObjects[i] = $(this.nodes[i]);
  }

  // Set up dimensions
  this.parentWidth = this.parent.width();
  this.ellipseWidth = this.parentWidth - viewsTurntableSettings.nodeWidth - 1;
  this.nodes.css(
    {
      position: 'absolute',
      width: viewsTurntableSettings.nodeWidth + 'px'
    }
  );
  this.parentHeight = parseInt(viewsTurntableSettings.ellipseHeight) + this.maxNodeHeight();
  this.parent.css({position: 'relative', height: this.parentHeight + 'px'});
  this.sectorSize = 360 / this.nodes.length;
  this.displayCenterX = (this.parentWidth - viewsTurntableSettings.nodeWidth)/2;
  this.displayCenterY = viewsTurntableSettings.ellipseHeight/2;
  this.ellipseRadiusX = this.ellipseWidth/2;
  this.ellipseRadiusY = viewsTurntableSettings.ellipseHeight/2;

  // Initialise optimizations
  this.offsetAngle = new Array();
  for (i = 0; i < this.nodes.length; i++){
    this.offsetAngle[i] = 360 * i / this.nodes.length;
  }
  this.positions = new Array();
  for (i = 0; i < 360; i+= 0.25){
    theta = i.toString();
    this.positions[theta] = new Array();
    this.positions[theta]['left'] = parseInt(this.displayCenterX - sinTable[theta] * this.ellipseRadiusX) + 'px';
    this.positions[theta]['opacity'] = cosTable[theta] * 0.3 + 0.66; 
    this.positions[theta]['top'] = parseInt(this.displayCenterY + cosTable[theta] * this.ellipseRadiusY) + 'px';
    this.positions[theta]['z-index'] = parseInt((cosTable[theta] + 1) * 100); 
  }

  // Initialise animation variables
  this.animQueue = new Array();
  this.animating = false;

  // Initialise behaviour
  switch(viewsTurntableSettings.spinType){
    case 'button':
      this.buttonBehaviour();
      break;
    case 'mousehover': 
      this.hoverBehaviour();
      this.goingForward = false;
      this.goingBackward = false;
      break;
  }

  // Set up display
  this.currentNode = 0;
  this.displayAngle(0);
}



/* ---- Behaviours ---- */



NodeRotator.prototype.buttonBehaviour = function(){
  this.parent.prepend(
    '<li class="turntable-button"><a nohref class="turntable-prev"><span>&lt; Previous</span></a></li><li class="turntable-button"><a nohref class="turntable-next"><span>Next &gt;</span></a></li>'
  );

  var prevButton = $('a.turntable-prev', this.parent);
  var nextButton = $('a.turntable-next', this.parent);
  var thisObject = this;

  prevButton.click(
    function(){
        thisObject.displayPrev(NodeRotator.profileSmooth);
      return false;
    }
  );

  nextButton.click(
    function(){
      thisObject.displayNext(NodeRotator.profileSmooth);
      return false;
    }
  );
}



NodeRotator.prototype.hoverBehaviour = function(){

  var thisObject = this;

  this.parent.mouseover(
    function(event){

      var x = event.pageX - thisObject.parent.offset().left;
      var width = thisObject.parent.width();

      switch (true){

        case x < width / 4:
          if (!thisObject.goingBackward){
            thisObject.goingBackward = true;
            thisObject.displayPrev(NodeRotator.profileSmoothStart);
            thisObject.parent.bind('lastFrame', function(){ thisObject.displayPrev(NodeRotator.profileFlat); });
          }
        break;

        case x > width / 4 && x < width * 3 / 4:
          thisObject.goingBackward = false;
          thisObject.goingForward = false;
          thisObject.parent.unbind('lastFrame');
        break;

        case x > width * 2 / 4:
          if (!thisObject.goingForward){
            thisObject.goingForward = true;
            thisObject.displayNext(NodeRotator.profileSmoothStart);
            thisObject.parent.bind('lastFrame', function(){ thisObject.displayNext(NodeRotator.profileFlat); });
          }
        break;
      }

    }    
  );
  
  this.parent.bind('mouseleave',
    function(){
      thisObject.parent.unbind('lastFrame');
      thisObject.goingBackward = false;
      thisObject.goingForward = false;
    }
  );
  
}



NodeRotator.prototype.dragBehaviour = function(){

  this.nodes.mousedown(
    function(){}
  );

}


/* ---- animating ---- */



NodeRotator.prototype.displayNext = function(profile){
  
  var newNode = (this.currentNode + 1) % this.nodes.length;
  var currentAngle = this.offsetAngle[this.currentNode];
  var nextAngle = this.offsetAngle[newNode];

  if (currentAngle > nextAngle) {
    nextAngle+= 360;
  }

  var range = nextAngle - currentAngle;

  for (angle = currentAngle; angle <= nextAngle; angle += profile[parseInt((angle - currentAngle) / range * profile.length)]){
    this.enqueueFrame(this.renderFrameInfo(angle));
  }

  this.play();
  this.currentNode = newNode;
}



NodeRotator.prototype.displayPrev = function(profile){
  
  var newNode = (this.currentNode + this.nodes.length - 1) % this.nodes.length;
  var currentAngle = this.offsetAngle[this.currentNode] + 360;
  var nextAngle = this.offsetAngle[newNode] + 360;

  if (currentAngle < nextAngle){
    currentAngle += 360;
  }

  var range = currentAngle - nextAngle;

  for (angle = currentAngle; angle >= nextAngle; angle -= profile[parseInt((currentAngle - angle) / range * profile.length)]){
    this.enqueueFrame(this.renderFrameInfo(angle));
  }

  this.play();
  this.currentNode = newNode;
}



NodeRotator.prototype.displayAngle = function(angle){

  var frameInfo = this.renderFrameInfo(angle);

  for(var index = 0; index < this.nodes.size(); index++){
    this.nodeObjects[index].css(frameInfo[index]);
  }
}



NodeRotator.prototype.renderFrameInfo = function(angle){

  displayInfo = new Array();

  for(var index = 0; index < this.nodes.size(); index++){

    var theta = ((parseInt(4 * (this.sectorSize * index + angle)) / 4) % 360).toString();

    displayInfo[index] =
     {
        left: this.positions[theta]['left'],
        opacity: this.positions[theta]['opacity'],
        top: this.positions[theta]['top'],
        zIndex: this.positions[theta]['z-index']
      };

  }

  return displayInfo;
}



NodeRotator.prototype.play = function(){

  if (!this.animating){
    this.animating = true;
    this.renderFrame();
  }

}



NodeRotator.prototype.renderFrame = function(){

  if (this.animQueue.length > 0){
    frameInfo = this.animQueue.shift();
    for (i = 0; i < frameInfo.length; i++){
      this.nodeObjects[i].css(frameInfo[i]);
    }
  }

  if (this.animQueue.length == 1){
    this.parent.trigger('lastFrame');
  }

  if (this.animQueue.length > 0){

    thisObject = this;
    var renderFrame = function(){ thisObject.renderFrame(); };
    setTimeout(renderFrame, 150 - viewsTurntableSettings.spinSpeed * 10);

  } else {
    this.animating = false;
  }

}



NodeRotator.prototype.enqueueFrame = function(frame){
  this.animQueue.push(frame);
}



/* ---- Utility ---- */



NodeRotator.prototype.maxNodeHeight = function(){

  var maxHeight = 0;

  for (var index=0; index < this.nodes.length - 1; index++){
    var node =  this.nodeObjects[index];
    maxHeight = Math.max(maxHeight, 
      node.height() +
      parseInt(node.css('padding-top')) +
      parseInt(node.css('padding-bottom')) +
      parseInt(node.css('border-top-width')) +
      parseInt(node.css('border-bottom-width'))
    );
  }

  return maxHeight;
}



