<?php



/**
  * Implementation of hook_views_plugin().
  */
function views_turntable_views_plugins() {
  return array(
    'module' => 'views_turntable',
    'style' => array(
      'views_turntable' => array(
        'title' => t('Turntable'),
        'theme' => 'views_turntable_view',
        'help' => t('Display rows in a rotatble disk.'),
        'handler' => 'views_turntable_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}



