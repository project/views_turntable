<?php


// Provides default options and form


class views_turntable_style_plugin extends views_plugin_style {



  function option_definition() {

    $options = parent::option_definition();

    $options['rear_opacity'] = array('default' => 0);
    $options['front_opacity'] = array('default' => 1);
    $options['enabled_sector'] = array('default' => 180);
    $options['idle_spin_speed'] = array('default' => 0);
    $options['idle_interval'] = array('default' => 0);
    $options['spin_type'] = array('default' => 'hover');
    $options['spin_speed'] = array('default' => 5);
    $options['node_width'] = array('default' => 200);
    $options['ellipse_height'] = array('default' => 200);
    
    return $options;
  }



  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    $opacity_options = range(0, 1, 0.05);
    $opacity_options = array_combine($opacity_options, $opacity_options);
    
    $idle_speed_options = range(0, 10, 1);
    $idle_speed_options = array_combine($idle_speed_options, $idle_speed_options);
    
    $speed_options = range(1, 10, 1);
    $speed_options = array_combine($speed_options, $speed_options);

    $interval_options = range(0, 5000, 50);
    $interval_options = array_combine($interval_options, $interval_options);

    $sector_options = range(0, 360, 5);
    $sector_options = array_combine($sector_options, $sector_options);

//    $form['rear_opacity'] = array(
//      '#type' => 'select',
//      '#title' => t('Rear opacity'),
//      '#default_value' => $this->options['rear_opacity'],
//      '#options' => $opacity_options,
//    );
//
//    $form['front_opacity'] = array(
//      '#type' => 'select',
//      '#title' => t('Front opacity'),
//      '#default_value' => $this->options['front_opacity'],
//      '#options' => $opacity_options,
//    );
//
//    $form['enabled_sector'] = array(
//      '#type' => 'select',
//      '#title' => t('Enabled sector'),
//      '#default_value' => $this->options['enabled_sector'],
//      '#options' => $sector_options,
//      '#description' => 'The size, in degrees, of the section where nodes are clickable',
//    );
//
//    $form['idle_spin_speed'] = array(
//      '#type' => 'select',
//      '#title' => t('Spin speed when Idle'),
//      '#default_value' => $this->options['idle_spin_speed'],
//      '#options' => $idle_speed_options,
//      '#description' => 'The speed at which the turntable should rotate when idle',
//    );
//
//    $form['idle_interval'] = array(
//      '#type' => 'select',
//      '#title' => t('Interval until idle'),
//      '#default_value' => $this->options['idle_interval'],
//      '#options' => $interval_options,
//      '#description' => 'The interval of user inaction before the turntable goes into idle mode',
//    );

    $form['spin_type'] = array(
      '#type' => 'select',
      '#title' => t('Spin when the user'),
      '#default_value' => $this->options['spin_type'],
      '#options' => array('button' => 'Clicks a button', /*'mouseclick' => 'Clicks the mouse',*/ 'mousehover' => 'Hovers the mouse'),
    );

    $form['spin_speed'] = array(
      '#type' => 'select',
      '#title' => t('Spin speed'),
      '#default_value' => $this->options['spin_speed'],
      '#options' => $speed_options,
      '#description' => 'The speed at which the turntable rotates on user action',
    );

    $form['node_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Node width'),
      '#default_value' => $this->options['node_width'],
      '#description' => 'The display width of nodes in pixels',
    );

    $form['ellipse_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Ellipse height'),
      '#default_value' => $this->options['ellipse_height'],
      '#description' => 'The height of the elliptical path of nodes, in pixels',
    );
  }



}